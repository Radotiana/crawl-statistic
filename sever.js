var express = require ('express');
var App = express ();
const neo4j = require ('neo4j-driver');
var favicon = require ('serve-favicon');
var path = require ('path');
var cors=require('cors');
App.use(cors());
App.use (favicon (path.join (__dirname, 'public', 'favicon.ico')));

const driver = new neo4j.driver (
  'bolt://172.17.7.250:7687/',
  neo4j.auth.basic ('neo4j', 'test')
);
const session = driver.session ();

App.use ('/', express.static ('FrontEnd'));

//FONCTION QUI RETOURNE L'ID DE L'OPERATION PAR RAPPORT A L'URL
async function getOperationIdByUrl (url, res) {
  return new Promise ((resolve, reject) => {
    const cypher =
      'MATCH (o:Operation) WHERE o.targetUrl="' + url + '" RETURN o';
    session
      .run (cypher)
      .then (result => {
        const response = result.records;
        if (response.length == 0) {
          throw new Error ('Operation not exist');
        }
        reponse = response[0].get ('o').properties.id;
        resolve (reponse);
      })
      .catch (e => {
        console.error (e);
        res.status (404);
        res.send ({error: e.toString ()});
      });
  });
}

//FONCTION QUI RETOURNE L'URL DE L'OPERATION PAR RAPPORT A L'ID
async function getOperationUrlById (id, res) {
  return new Promise ((resolve, reject) => {
    const cypher = 'MATCH (o:Operation) WHERE o.id="' + id + '" RETURN o';
    session
      .run (cypher)
      .then (result => {
        const response = result.records;
        if (response.length == 0) {
          throw new Error ('Operation not exist');
        }
        reponse = response[0].get ('o').properties.targetUrl;
        resolve (reponse);
      })
      .catch (e => {
        res.status (404);
        res.send ({error: e.toString ()});
      });
  });
}

//PAGE D'ACCUEIL
App.get ('/getOperation', async function (req, res) {
  const sessions = driver.session ();
  const cy =
    'MATCH path=(o:Operation)-[:CHILD*1..1]->(childs) WHERE o.id="root" return count(*) as c';
  var count;
  sessions
    .run (cy)
    .then (results => {
      count = results.records[0].get ('c');
      const cypher =
        'MATCH path = (o:Operation)-[:CHILD]-(childs) WHERE o.id="root" UNWIND nodes(path) as p UNWIND relationships(path) as r RETURN {nodes: collect(distinct {id: childs.id, label: childs.targetUrl}), links: collect(DISTINCT {from: startNode(r).id, to: endNode(r).id})}';
      sessions.run (cypher).then (result => {
        result.records[0]._fields.push ({id: 'root', label: 'root'});
        result.records[0]._fields.push (count);
        res.status (200).send ({message: result.records[0]._fields});
      });
    })
    .catch (e => {
      res.status (500).send ({error: e});
    });
});

//FONCTION QUI RETOURNE LES DETAILS D'UN OPERATION PAR RAPPORT A SON URL
App.get ('/getOperationDetails', async function (req, res) {
  const sessions = driver.session ();
  var label = req.query.data;
  var id;
  var reponse = [];
  if (label.includes ('https://')) {
    var operationId = await getOperationIdByUrl (label, res);
    id = operationId;
  } else {
    id = label;
  }
  const cypher =
    'MATCH (s:Statistic)-[:STAT]-(o:Operation)-[:HTTP]-(n:Connectivity) WHERE o.id="' +
    id +
    '" RETURN n,s';
  sessions
    .run (cypher)
    .then (result => {
      const response = result.records;
      if (response[0] !== undefined) {
        reponse.push (response[0].get ('n').properties);
        reponse.push (response[0].get ('s').properties);
      } else {
        reponse.push ({});
      }
      res.status (200).send ({message: reponse});
    })
    .catch (e => {
      res.status (500).send ({error: e});
    });
});

//FONCTION QUI RETOURNE LES PARENTS D'UN OPERATION PAR RAPPORT A SON URL
App.get ('/getOperationParent', async function (req, res) {
  const sessions = driver.session ();
  var label = req.query.data;
  var level = req.query.level;
  var id;
  if (label.includes ('https://')) {
    var operationId = await getOperationIdByUrl (label, res);
    id = operationId;
  } else {
    id = label;
    label = await getOperationUrlById (label, res);
  }
  const cypher =
    'match path = (o:Operation)<-[:PARENT*1..' +
    level +
    ']-(c:Operation) where c.id="' +
    id +
    '" UNWIND nodes(path) as p UNWIND relationships(path) as r return { nodes : collect(distinct{id: o.id, label: o.targetUrl}), links: collect(distinct{from:startNode(r).id, to:endNode(r).id})}';
  sessions
    .run (cypher)
    .then (result => {
      result.records[0]._fields.push ({id, label});
      res.status (200).send ({message: result.records[0]._fields});
    })
    .catch (e => {
      res.status (500).send ({error: e.toString ()});
    });
});

//FONCTION QUI RETOURNE LES PARENTS D'UN OPERATION PAR RAPPORT A SON URL
App.get ('/getOperationChild', async function (req, res) {
  const sessions = driver.session ();
  var label = req.query.data;
  var level = req.query.level;
  var page = req.query.page;
  page = page * 100;
  var id;
  if (label.includes ('https://')) {
    var operationId = await getOperationIdByUrl (label, res);
    id = operationId;
  } else {
    id = label;
    label = await getOperationUrlById (label, res);
  }

  const cy =
    'MATCH path=(o:Operation)-[:CHILD*1..' +
    level +
    ']->(childs) WHERE o.id="' +
    id +
    '" return count(*) as c';
  var count;
  sessions
    .run (cy)
    .then (results => {
      count = results.records[0].get ('c');
      limit = 100;
      if (id == 'root') {
        limit = 350;
      }
      const cypher =
        'MATCH path=(o:Operation)-[:CHILD*1..' +
        level +
        ']->(childs) WHERE o.id="' +
        id +
        '" UNWIND nodes(path) as p UNWIND relationships(path) as r with r, childs  skip ' +
        page +
        ' limit ' +
        limit +
        ' return { nodes : collect(distinct{id: childs.id, label: childs.targetUrl}), links: collect(distinct{from:startNode(r).id, to:endNode(r).id})}';
      sessions.run (cypher).then (result => {
        result.records[0]._fields.push ({id, label});
        result.records[0]._fields.push (count);
        res.status (200).send ({message: result.records[0]._fields});
      });
    })
    .catch (e => {
      res.status (500).send ({error: e.toString ()});
    });
});

//INITIALISATION DU PORT DU SERVEUR
const PORT = process.env.PORT || 3000;
App.listen (PORT, () => {
  console.log (`Our app is running on port ${PORT}`);
});
