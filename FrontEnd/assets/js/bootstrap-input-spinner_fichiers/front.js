AppRoute.controller("front", accueil);
function getIdByUrl(url) {
    return new Promise((resolve, reject) => {
        const cypher = 'MATCH (o:Operation) WHERE o.targetUrl="' + url + '" RETURN o';
        console.log("requete: " + cypher);
        session.run(cypher).then(result => {
            const response = result.records;
            reponse = response[0].get('o').properties.id;
            resolve(reponse);
        })
            .catch(e => {
                console.log(e);
            })
    })
}

function accueil($scope, $http, $cookies, $location, $window) {
    $scope.test = function (event) {
        $scope.loader = true;
        var button = event.currentTarget;
        var buttonId = angular.element(button).attr('id');
        var data = $scope.mot;
        var nombre = $scope.nombre;
        console.log(nombre);
        if(nombre==undefined)
        {
            nombre=0;
        }
        if (data==undefined) {
            data=root;
        }
        if (data == undefined) {
            data = document.getElementById("mots").value;
        }
        var funct = "getParent";
        if (buttonId == "enfant") {
            funct = "getChild";
        }
        $http({
            method: 'GET',
            url: '/' + funct + '?data=' + encodeURIComponent(data) + '&level=' + nombre
        }).then(function successCallback(response) {
            $scope.loader = false;

            var nod = response.data.message[0].nodes;
            var lin = response.data.message[0].links;

            var disposition = "LR";
            response.data.message[0].nodes.push(response.data.message[1]);

            var nodes = new vis.DataSet(response.data.message[0].nodes);
            var edges = new vis.DataSet(response.data.message[0].links);

            var container = document.getElementById("mynetwork");
            var data = {
                nodes: nodes,
                edges: edges,
            };
            var options = {
                physics: false,

                layout: {
                    hierarchical: {
                        direction: disposition,
                        sortMethod: "directed",
                        treeSpacing: 200,
                        levelSeparation: 800,
                        nodeSpacing: 70,
                    },
                },

                edges: {
                    arrows: "to",
                    smooth: {
                        type: "cubicBezier",
                    },
                },
                autoResize: true,
                clickToUse: true,
                nodes: {
                    shape: 'dot',
                    size: 10,
                    font: { size: 25, color: 'white' },
                }
            };

            var network = new vis.Network(container, data, options);
            network.moveTo({ scale: 0.5 });
            network.on('click', function (properties) {
                selection = properties.nodes
                var nodeId = properties.nodes[0];
                if (nodeId) {
                    var clickedNode = this.body.nodes[nodeId];
                    document.getElementById("mots").value = clickedNode.options.label;
                }
            });

        }, function errorCallback(response) {
            alert(response.data.error);
            location.reload();
        });
    }
    $scope.init = function () {
        $scope.loader = true;
        $http({
            method: 'GET',
            url: '/getOperation'
        }).then(function successCallback(response) {
            $scope.loader = false;
            console.log(response.data);
            response.data.message[0].nodes.push(response.data.message[1]);
            var nodes = new vis.DataSet(response.data.message[0].nodes);
            var edges = new vis.DataSet(response.data.message[0].links);
            var container = document.getElementById("mynetwork");
            var data = {
                nodes: nodes,
                edges: edges,
            };

            var options = {
                physics: false,

                layout: {
                    hierarchical: {
                        direction: "LR",
                        sortMethod: "directed",
                        treeSpacing: 200,
                        levelSeparation: 800,
                        nodeSpacing: 70,
                    },
                },

                edges: {
                    arrows: "to",
                    smooth: {
                        type: "cubicBezier",
                    },
                },
                autoResize: true,
                clickToUse: true,
                nodes: {
                    shape: 'dot',
                    size: 10,
                    font: { size: 25, color: 'white' },
                }
            };

            var network = new vis.Network(container, data, options);
            network.moveTo({ scale: 0.5 });


            network.on('click', function (properties) {
                selection = properties.nodes
                var nodeId = properties.nodes[0];
                if (nodeId) {
                    var clickedNode = this.body.nodes[nodeId];
                    document.getElementById("mots").value = clickedNode.options.label;
                }
            });
        }, function errorCallback(response) {
            console.log(response.data.error);
        });
    }
    $scope.actualiser = function () {
        location.reload();
    }
}

