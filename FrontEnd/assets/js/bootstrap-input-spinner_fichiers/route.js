let AppRoute=angular.module("NH",['ngRoute','ngCookies']);


AppRoute.config(function($routeProvider){
    $routeProvider
    .when('/recherche',{
        templateUrl: 'view/recherche.html',
        controller: 'front'
    })
    .when('/Madagascar-diving-underwater',{
        templateUrl: 'view/accueil.html',
        controller: 'front'
    })
    .when('/Madagascar-diving-underwater/admin',{
        templateUrl: 'view/backoffice.html',
        controller: 'front'
    })
    .when('/Madagascar-diving-underwater/admin/acceuil',{
        templateUrl: 'view/backoffice_acceuil.html',
        controller: 'back'
    })
    .otherwise({redirectTo:'/recherche'});
});