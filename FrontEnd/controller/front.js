AppRoute.controller ('front', accueil);
function accueil ($scope, $http) {
  var url;
  var levels;
  var type;
  var inps;
  $scope.test = function (event) {
    $scope.loader = true;
    var button = event.currentTarget;
    var buttonId = angular.element (button).attr ('id');
    var number = $scope.number;
    if (number == undefined) {
      number = 1;
    }
    var data = document.getElementById ('words').value;
    if (data == undefined || data == '') {
      data = 'root';
    }

    var functions = 'getOperationParent';
    if (buttonId == 'words') {
      functions = 'getOperationChild';
    }
    if (buttonId == 'parent') {
      number = 50;
    }
    if (buttonId == 'child') {
      functions = 'getOperationChild';
    }
    var page = 0;
    document.getElementById (page).className += ' disabled active';
    document.getElementById (page).style.cursor = 'no-drop';
    url = data;
    levels = number;
    type = functions;
    $http ({
      method: 'GET',
      url: '/' +
        functions +
        '?data=' +
        encodeURIComponent (data) +
        '&level=' +
        number +
        '&page=' +
        page,
    }).then (
      function successCallback (response) {
        $http ({
          method: 'GET',
          url: '/getOperationDetails?data=' + encodeURIComponent (url),
        }).then (
          function successCallback (response) {
            $scope.connectivity = response.data.message[0];
            $scope.statistic = response.data.message[1];
          },
          function errorCallback (response) {
            alert (response.data.error);
          }
        );
        var size = 0;
        if (functions == 'getOperationChild') {
          size = response.data.message[2].low;
        }
        $scope.loader = false;
        var paginate = Math.round (size / 50);
        $scope.range = function () {
          var input = [];
          for (var i = 0; i <= paginate; i += 1) {
            input.push (i);
          }
          inps = input;
          return input;
        };
        var node = response.data.message[0].nodes;
        node.push (response.data.message[1]);
        var link = response.data.message[0].links;

        var nodes = new vis.DataSet (node);
        var edges = new vis.DataSet (link);

        var container = document.getElementById ('mynetwork');
        var data = {
          nodes: nodes,
          edges: edges,
        };
        var options = {
          physics: false,

          layout: {
            hierarchical: {
              direction: 'LR',
              sortMethod: 'directed',
              treeSpacing: 500,
              levelSeparation: 1500,
              nodeSpacing: 100,
            },
          },

          edges: {
            arrows: 'to',
            smooth: {
              type: 'cubicBezier',
            },
          },
          autoResize: true,
          clickToUse: true,
          nodes: {
            shape: 'dot',
            size: 15,
            font: {size: 25, color: 'white'},
          },
        };

        var network = new vis.Network (container, data, options);
        network.moveTo ({scale: 0.4});
        network.on ('click', function (properties) {
          selection = properties.nodes;
          var nodeId = properties.nodes[0];
          if (nodeId) {
            var clickedNode = this.body.nodes[nodeId];
            document.getElementById ('words').value = clickedNode.options.label;
            $http ({
              method: 'GET',
              url: '/getOperationDetails?data=' +
                encodeURIComponent (clickedNode.options.label),
            }).then (
              function successCallback (response) {
                $scope.connectivity = response.data.message[0];
                $scope.statistic = response.data.message[1];
              },
              function errorCallback (response) {
                alert (response.data.error);
              }
            );
          }
        });
        network.on ('doubleClick', function (properties) {
          selection = properties.nodes;
          var nodeId = properties.nodes[0];
          if (nodeId) {
            var clickedNode = this.body.nodes[nodeId];
            window.open (clickedNode.options.label);
          }
        });

        $scope.paginate = function (event) {
          document.getElementById (0).className = 'page-item';
          document.getElementById (event).className += ' active disabled';
          document.getElementById (event).style.cursor = 'no-drop';

          console.log (document.getElementById (event));
          for (var i = 0; i < inps.length; i++) {
            if (i != event) {
              document.getElementById (i).className = 'page-item';
            }
          }

          $http ({
            method: 'GET',
            url: '/' +
              type +
              '?data=' +
              encodeURIComponent (url) +
              '&level=' +
              levels +
              '&page=' +
              event,
          }).then (
            function successCallback (response) {
              var size = 0;
              if (functions == 'getOperationChild') {
                size = response.data.message[2].low;
              }
              $scope.loader = false;
              var paginate = Math.round (size / 50);
              $scope.range = function () {
                var input = [];
                for (var i = 0; i <= paginate; i += 1) {
                  input.push (i);
                }
                return input;
              };
              var node = response.data.message[0].nodes;
              node.push (response.data.message[1]);
              var link = response.data.message[0].links;

              var nodes = new vis.DataSet (node);
              var edges = new vis.DataSet (link);

              var container = document.getElementById ('mynetwork');
              var data = {
                nodes: nodes,
                edges: edges,
              };
              var options = {
                physics: false,

                layout: {
                  hierarchical: {
                    direction: 'LR',
                    sortMethod: 'directed',
                    treeSpacing: 200,
                    levelSeparation: 1500,
                    nodeSpacing: 70,
                  },
                },

                edges: {
                  arrows: 'to',
                  smooth: {
                    type: 'cubicBezier',
                  },
                },
                autoResize: true,
                clickToUse: true,
                nodes: {
                  shape: 'dot',
                  size: 15,
                  font: {size: 25, color: 'white'},
                },
              };

              var network = new vis.Network (container, data, options);
              network.moveTo ({scale: 0.4});
              network.on ('click', function (properties) {
                selection = properties.nodes;
                var nodeId = properties.nodes[0];
                if (nodeId) {
                  var clickedNode = this.body.nodes[nodeId];
                  document.getElementById ('words').value =
                    clickedNode.options.label;
                  $http ({
                    method: 'GET',
                    url: '/getOperationDetails?data=' +
                      encodeURIComponent (clickedNode.options.label),
                  }).then (
                    function successCallback (response) {
                      $scope.connectivity = response.data.message[0];
                      $scope.statistic = response.data.message[1];
                    },
                    function errorCallback (response) {
                      alert (response.data.error);
                    }
                  );
                }
              });
              network.on ('doubleClick', function (properties) {
                selection = properties.nodes;
                var nodeId = properties.nodes[0];
                if (nodeId) {
                  var clickedNode = this.body.nodes[nodeId];
                  window.open (clickedNode.options.label);
                }
              });
            },
            function errorCallback (response) {
              alert (response.data.error);
            }
          );
        };
      },
      function errorCallback (response) {
        alert (response.data.error);
        location.reload ();
      }
    );
  };

  $scope.init = function () {
    $scope.loader = true;
    $http ({
      method: 'GET',
      url: '/getOperation',
    }).then (
      function successCallback (response) {
        console.log (response.data);
        $scope.loader = false;
        var paginate = Math.round (response.data.message[2].low / 50);
        $scope.range = function () {
          var input = [];
          for (var i = 0; i <= paginate; i += 1) {
            input.push (i);
          }
          return input;
        };
        $scope.paginate = function (event) {};
        response.data.message[0].nodes.push (response.data.message[1]);
        var nodes = new vis.DataSet (response.data.message[0].nodes);
        var edges = new vis.DataSet (response.data.message[0].links);
        var container = document.getElementById ('mynetwork');
        var data = {
          nodes: nodes,
          edges: edges,
        };

        var options = {
          physics: false,

          layout: {
            hierarchical: {
              direction: 'LR',
              sortMethod: 'directed',
              treeSpacing: 200,
              levelSeparation: 800,
              nodeSpacing: 70,
            },
          },

          edges: {
            arrows: 'to',
            smooth: {
              type: 'cubicBezier',
            },
          },
          autoResize: true,
          clickToUse: true,
          nodes: {
            shape: 'dot',
            size: 15,
            font: {size: 25, color: 'white'},
          },
        };

        network = new vis.Network (container, data, options);
        network.moveTo ({scale: 0.4});

        network.on ('click', function (properties) {
          selection = properties.nodes;
          var nodeId = properties.nodes[0];
          if (nodeId) {
            var clickedNode = this.body.nodes[nodeId];
            document.getElementById ('words').value = clickedNode.options.label;
            $http ({
              method: 'GET',
              url: '/getOperationDetails?data=' +
                encodeURIComponent (clickedNode.options.label),
            }).then (
              function successCallback (response) {
                $scope.connectivity = response.data.message[0];
                $scope.statistic = response.data.message[1];
              },
              function errorCallback (response) {
                alert (response.data.error);
              }
            );
          }
        });
        network.on ('doubleClick', function (properties) {
          selection = properties.nodes;
          var nodeId = properties.nodes[0];
          if (nodeId) {
            var clickedNode = this.body.nodes[nodeId];
            window.open (clickedNode.options.label);
          }
        });
      },
      function errorCallback (response) {
        if (response.data.error.code == 'ServiceUnavailable') {
          alert (
            response.data.error.name +
              ': ' +
              response.data.error.code +
              ', Connection TIMED OUT'
          );
        } else {
          alert (response.data.error);
        }
        $scope.loader = false;
      }
    );
  };
  $scope.actualiser = function () {
    location.reload ();
  };
}
