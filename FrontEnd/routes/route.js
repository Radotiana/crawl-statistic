let AppRoute=angular.module("NH",['ngRoute','ngCookies']);


AppRoute.config(function($routeProvider){
    $routeProvider
    .when('/',{
        templateUrl: 'view/recherche.html',
        controller: 'front'
    })
    .otherwise({redirectTo:'/'});
});